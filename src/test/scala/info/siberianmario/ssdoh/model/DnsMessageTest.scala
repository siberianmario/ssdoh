package info.siberianmario.ssdoh.model

import akka.util.ByteString
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.util.Base64

class DnsMessageTest extends AnyWordSpec with Matchers {
  "DnsMessage" should {
    "deserialize request from binary" in {
      val bytes = Base64.getDecoder.decode("q80BAAABAAAAAAAAA3d3dwdleGFtcGxlA2NvbQAAAQAB")

      val message = DnsMessage.fromBinary(ByteString(bytes))

      message shouldBe DnsMessage(
        header = DnsHeader(id = 0xabcd, qdCount = 1),
        questions = Seq(DnsQuestion("www.example.com", 1, 1)),
        answers = Seq.empty
      )
    }

    "serialize response to binary" in {
      val expected = Base64.getDecoder.decode("q82BgAABAAEAAAAAA3d3dwdleGFtcGxlA2NvbQAAAQABwAwAAQABAAEmiQAEXbjYIg==")

      val message = DnsMessage(
        header = DnsHeader(id = 0xabcd, qr = true, ra = true, qdCount = 1, anCount = 1),
        questions = Seq(DnsQuestion("www.example.com", 1, 1)),
        answers = Seq(
          DnsAnswer(
            name = "www.example.com",
            aType = 1,
            aClass = 1,
            ttl = 0x0012689, // 75401
            rdLength = 4,
            rData = Seq(0x5d, 0xb8, 0xd8, 0x22).map(_.toByte).toArray // 93.184.216.34
          )
        )
      )

      DnsMessage.toBinary(message).toArray shouldBe expected
    }
  }
}
