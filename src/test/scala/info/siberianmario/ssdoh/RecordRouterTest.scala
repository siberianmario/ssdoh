package info.siberianmario.ssdoh

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import info.siberianmario.ssdoh.actor.RegistryService
import info.siberianmario.ssdoh.model.{Record, UpdateRecord}
import info.siberianmario.ssdoh.router.RecordRouter
import org.scalamock.scalatest.MockFactory
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json._

import scala.concurrent.Future

class RecordRouterTest extends AnyWordSpec with Matchers with ScalatestRouteTest with MockFactory {

  private val testRecord = Record(name = "test.com", data = "1.1.1.1", ttl = 255)

  trait Routes extends RecordRouter {
    override val registryService: RegistryService = mock[RegistryService]
    val routes: Route = Route.seal(recordRoutes)
  }

  "RecordRoutes" should {
    "return list of records (GET /records)" in new Routes {
      (registryService.getAllRecords _).expects().returning(Future.successful(Seq(testRecord))).once()

      val result = Get("/records") ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`application/json`
        entityAs[String] shouldBe """[{"data":"1.1.1.1","name":"test.com","ttl":255}]"""
      }(result)
    }

    "return no records if no present (GET /records)" in new Routes {
      (registryService.getAllRecords _).expects().returning(Future.successful(Seq.empty)).once()

      val result = Get("/records") ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`application/json`
        entityAs[String] shouldBe """[]"""
      }(result)
    }

    "return record by name (GET /records/{name})" in new Routes {
      (registryService.getRecord _).expects("test.com").returning(Future.successful(Some(testRecord))).once()

      val result = Get("/records/test.com") ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`application/json`
        entityAs[String] shouldBe """{"data":"1.1.1.1","name":"test.com","ttl":255}"""
      }(result)
    }

    "reject to get record by name if record doesn't exist (GET /records/{name})" in new Routes {
      (registryService.getRecord _).expects("another.net").returning(Future.successful(None)).once()

      val result = Get("/records/another.net") ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.NotFound
        contentType shouldBe ContentTypes.`text/plain(UTF-8)`
        responseAs[String] shouldBe "The requested resource could not be found."
      }(result)
    }

    "be able to add records (POST /records)" in new Routes {
      (registryService.createRecord _).expects(testRecord).returning(Future.successful(testRecord)).once()

      val request = Post("/records").withEntity(ContentTypes.`application/json`, testRecord.toJson.toString)
      val result = request ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.Created
        contentType shouldBe ContentTypes.`application/json`
        entityAs[String] shouldBe """{"data":"1.1.1.1","name":"test.com","ttl":255}"""
      }(result)
    }

    "be able to update record by name (PUT /records/{name})" in new Routes {
      (registryService.updateRecord _).expects(testRecord).returning(Future.successful(Some(testRecord))).once()

      val request = Put("/records/test.com").withEntity(ContentTypes.`application/json`, testRecord.toJson.toString)
      val result = request ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`application/json`
        entityAs[String] shouldBe """{"data":"1.1.1.1","name":"test.com","ttl":255}"""
      }(result)
    }

    "reject to update record by name if record doesn't exist (PUT /records/{name})" in new Routes {
      val another = testRecord.copy(name = "another.net")
      val update = UpdateRecord(testRecord.data, testRecord.ttl)
      (registryService.updateRecord _).expects(another).returning(Future.successful(None)).once()

      val request = Put("/records/another.net").withEntity(ContentTypes.`application/json`, update.toJson.toString)
      val result = request ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.NotFound
        contentType shouldBe ContentTypes.`text/plain(UTF-8)`
        entityAs[String] shouldBe "The requested resource could not be found."
      }(result)
    }

    "be able to remove records (DELETE /records/{name})" in new Routes {
      (registryService.deleteRecord _).expects("test.com").returning(Future.successful(())).once()

      val result = Delete("/records/test.com") ~> routes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`text/plain(UTF-8)`
        entityAs[String] shouldBe "Record for domain test.com deleted."
      }(result)
    }
  }
}
