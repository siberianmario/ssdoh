package info.siberianmario.ssdoh

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{ContentType, ContentTypes, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import info.siberianmario.ssdoh.actor.DnsQueryService
import info.siberianmario.ssdoh.model._
import info.siberianmario.ssdoh.router.DnsQueryRouter
import org.scalamock.scalatest.MockFactory
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json._

import scala.concurrent.Future

class DnsQueryRouterTest extends AnyWordSpec with Matchers with ScalatestRouteTest with MockFactory {

  val testResponse: DnsResponseDto = {
    val question = DnsQuestionDto("test.com", 1)
    val answer = DnsAnswerDto(name = "test.com", `type` = 1, TTL = 255, data = "192.168.0.1")
    DnsResponseDto(0, TC = true, RD = true, RA = true, AD = false, CD = false, Seq(question), Seq(answer))
  }

  trait Routes extends DnsQueryRouter {
    override val dnsQueryService: DnsQueryService = mock[DnsQueryService]
  }

  "RecordRoutes" should {
    "return DNS record (GET /dns-query)" in new Routes {
      val request = Get("/dns-query?name=test.com&type=A").addHeader(RawHeader("Accept", "application/dns-json"))

      (dnsQueryService.getDnsRecordJson _)
        .expects("test.com", "A", None, None)
        .returning(Future.successful(testResponse))
        .once()

      val result = request ~> dnsQueryRoutes ~> runRoute

      check {
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentType(`application/dns-json`)
        entityAs[String].parseJson shouldBe """{
            | "AD":false,
            | "Answer":[
            |   {
            |     "TTL":255,
            |     "data":"192.168.0.1",
            |     "name":"test.com",
            |     "type":1
            |   }
            | ],
            | "CD":false,
            | "Question":[
            |   {
            |     "name":"test.com",
            |     "type":1
            |   }
            | ],
            | "RA":true,
            | "RD":true,
            | "Status":0,
            | "TC":true
            |}""".stripMargin.parseJson
      }(result)
    }

    "reject request if record doesn't exist (GET /records)" in new Routes {
      val request = Get("/dns-query?name=test.com&type=TXT").addHeader(RawHeader("Accept", "application/dns-json"))

      (dnsQueryService.getDnsRecordJson _)
        .expects("test.com", "TXT", None, None)
        .returning(Future.failed(new RuntimeException()))
        .once()

      val result = request ~> dnsQueryRoutes ~> runRoute

      check {
        status shouldBe StatusCodes.NotFound
        contentType shouldBe ContentTypes.`text/plain(UTF-8)`
        responseAs[String] shouldBe "Record not found"
      }(result)
    }
  }
}
