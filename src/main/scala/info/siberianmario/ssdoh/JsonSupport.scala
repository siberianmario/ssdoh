package info.siberianmario.ssdoh

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpCharsets
import akka.http.scaladsl.model.MediaType._
import info.siberianmario.ssdoh.model._
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  val `application/dns-message`: Binary = applicationBinary("dns-message", NotCompressible, "dns")
  val `application/dns-json`: WithFixedCharset = applicationWithFixedCharset("dns-json", HttpCharsets.`UTF-8`, "json")

  implicit val recordFormat: RootJsonFormat[Record] = jsonFormat3(Record)
  implicit val updateRecordFormat: RootJsonFormat[UpdateRecord] = jsonFormat2(UpdateRecord)

  implicit val dnsQuestionJsonFormat: RootJsonFormat[DnsQuestionDto] = jsonFormat2(DnsQuestionDto)
  implicit val dnsAnswerJsonFormat: RootJsonFormat[DnsAnswerDto] = jsonFormat4(DnsAnswerDto)
  implicit val dnsResponseJsonFormat: RootJsonFormat[DnsResponseDto] = jsonFormat8(DnsResponseDto)
}
