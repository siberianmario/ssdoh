package info.siberianmario.ssdoh.router

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteConcatenation._
import info.siberianmario.ssdoh.actor.{DnsQueryService, RegistryService}

case class RootEndpoint(dnsQueryService: DnsQueryService, registryService: RegistryService)
  extends DnsQueryRouter
  with RecordRouter {

  def routes: Route = recordRoutes ~ dnsQueryRoutes
}
