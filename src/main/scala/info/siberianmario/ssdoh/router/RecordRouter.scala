package info.siberianmario.ssdoh.router

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{delete, get, post}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import info.siberianmario.ssdoh.actor.RegistryService
import info.siberianmario.ssdoh.model.{Record, UpdateRecord}

trait RecordRouter extends BaseRouter {

  def registryService: RegistryService

  lazy val recordRoutes: Route =
    pathPrefix("records") {
      pathEnd {
        get {
          complete(registryService.getAllRecords())
        } ~ post {
          entity(as[Record]) { record =>
            onSuccess(registryService.createRecord(record)) { created =>
              complete(StatusCodes.Created, created)
            }
          }
        }
      } ~ path(Segment) { name =>
        get {
          rejectEmptyResponse {
            complete(registryService.getRecord(name))
          }
        } ~ put {
          entity(as[UpdateRecord]) { update =>
            val record = Record(name, update.data, update.ttl)
            onSuccess(registryService.updateRecord(record)) { updated =>
              rejectEmptyResponse {
                complete(updated)
              }
            }
          }
        } ~ delete {
          onSuccess(registryService.deleteRecord(name)) {
            complete(StatusCodes.OK, s"Record for domain $name deleted.")
          }
        }
      }
    }
}
