package info.siberianmario.ssdoh.router

import info.siberianmario.ssdoh.JsonSupport

import scala.concurrent.duration._

trait BaseRouter extends JsonSupport {
  implicit val timeout: FiniteDuration = 5.seconds
}
