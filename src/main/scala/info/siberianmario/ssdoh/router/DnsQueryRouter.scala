package info.siberianmario.ssdoh.router

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{get, post}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.stream.scaladsl.Source
import akka.util.ByteString
import info.siberianmario.ssdoh.actor.DnsQueryService
import info.siberianmario.ssdoh.model.DnsMessage
import spray.json._

import java.util.Base64
import scala.util.Success

trait DnsQueryRouter extends BaseRouter {

  def dnsQueryService: DnsQueryService

  lazy val dnsQueryRoutes: Route =
    path("dns-query") {
      get {
        headerValueByName("Accept") {
          case "application/dns-message" =>
            parameter("dns") { dns =>
              val bytes = Base64.getDecoder.decode(dns)
              val message = DnsMessage.fromBinary(ByteString(bytes))
              val response = dnsQueryService.getDnsRecordWireFormat(message)
              onComplete(response) {
                case Success(dnsMessage) =>
                  val binary = DnsMessage.toBinary(dnsMessage)
                  complete(HttpEntity(`application/dns-message`, binary.size, Source.single(binary)))
                case _ =>
                  complete(StatusCodes.NotFound, "Record not found")
              }
            }
          case "application/dns-json" =>
            parameters("name", "type", "do".?, "cd".?) { (name, recType, DO, CD) =>
              val response = dnsQueryService.getDnsRecordJson(name, recType, DO, CD)
              onComplete(response) {
                case Success(dnsResponseDto) =>
                  complete(HttpEntity(`application/dns-json`, dnsResponseDto.toJson.toString))
                case _ =>
                  complete(StatusCodes.NotFound, "Record not found")
              }
            }
          case _ =>
            complete(StatusCodes.NotAcceptable)
        }
      } ~ post {
        extractStrictEntity(timeout) { entity =>
          val message = DnsMessage.fromBinary(entity.data)
          val response = dnsQueryService.getDnsRecordWireFormat(message)
          onComplete(response) {
            case Success(dnsMessage) =>
              val binary = DnsMessage.toBinary(dnsMessage)
              complete(HttpEntity(`application/dns-message`, binary.size, Source.single(binary)))
            case _ =>
              complete(StatusCodes.NotFound, "Record not found")
          }
        }
      }
    }
}
