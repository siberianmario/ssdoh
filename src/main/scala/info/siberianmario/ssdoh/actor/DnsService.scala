package info.siberianmario.ssdoh.actor

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.http.scaladsl.unmarshalling.Unmarshal
import info.siberianmario.ssdoh.JsonSupport
import info.siberianmario.ssdoh.model._

import java.util.logging.Logger
import scala.concurrent.Future

class DnsService(implicit system: ActorSystem) extends JsonSupport {
  import system.dispatcher

  private val log = Logger.getLogger(getClass.getName)
  private val baseUrl = Uri("https://cloudflare-dns.com")

  def getDnsRecord(name: String, recordType: String): Future[Record] = {
    log.info(s"Requesting ext DNS for name = $name")
    val params = Map("name" -> name, "type" -> recordType)
    val request = Get(baseUrl.withPath(Path("dns-query")).withQuery(Query(params)))
      .withHeaders(Accept(`application/dns-json`))

    Http()
      .singleRequest(request)
      .flatMap {
        case resp if resp.status.isSuccess() =>
          Unmarshal(resp.entity).to[DnsResponseDto]
        case failed =>
          Future.failed(IllegalResponseException(failed.status.value))
      }
      .map { resp =>
        val answer = resp.Answer.head
        log.info(s"Received ext DNS record [name = ${answer.name}, ttl = ${answer.TTL}, data = ${answer.data}]")
        Record(answer.name, answer.data, answer.TTL)
      }
  }
}
