package info.siberianmario.ssdoh.actor

import info.siberianmario.ssdoh.model._

import java.util.logging.Logger
import scala.concurrent.{ExecutionContext, Future}

class DnsQueryService(cache: CacheService)(implicit ex: ExecutionContext) {

  private val log = Logger.getLogger(getClass.getName)

  def getDnsRecordJson(
    name: String,
    recordType: String,
    DO: Option[String] = None,
    CD: Option[String] = None
  ): Future[DnsResponseDto] = {
    log.info(s"received query: name = $name, type = $recordType")
    RecordType.getCode(recordType) match {
      case Some(typeCode) =>
        buildResponseDto(name, typeCode)
      case None =>
        Future.failed(new IllegalArgumentException(s"Not supported record type $recordType"))
    }
  }

  def getDnsRecordWireFormat(message: DnsMessage): Future[DnsMessage] = {
    log.info(s"received dnsWireFormat: ${message.toString}")
    val names = message.questions
      .filter(q => RecordType.getCode(q.qType.toString).nonEmpty)
      .map(_.qName)
    if (names.nonEmpty)
      cache.getCachedRecord(names.head).map { record =>
        val questions = message.questions
        val rData = record.data.split("\\.").map(_.toInt.toByte)
        val answers = Seq(DnsAnswer(record.name, 1, 1, record.ttl, rData.length, rData))
        val header = message.header.copy(qr = true, ra = true, anCount = answers.size, nsCount = 0, arCount = 0)
        val response = DnsMessage(header, questions, answers)
        log.info(s"responding with dnsWireFormat: ${response.toString}")
        response
      }
    else
      Future.successful(buildEmptyResponse(message))
  }

  private def buildEmptyResponse(message: DnsMessage): DnsMessage = {
    val header =
      message.header.copy(qr = true, rCode = 2, anCount = 0, nsCount = 0, arCount = 0) // Response Code - Server failure
    val response = DnsMessage(header, message.questions, Seq())
    log.info(s"error while fetching records, responding with dnsWireFormat: ${response.toString}")
    response
  }

  private def buildResponseDto(name: String, typeCode: Int): Future[DnsResponseDto] =
    cache.getCachedRecord(name).map { rec =>
      val question = DnsQuestionDto(name, typeCode)
      val answer = DnsAnswerDto(name = rec.name, `type` = typeCode, TTL = rec.ttl, data = rec.data)
      DnsResponseDto(0, TC = false, RD = true, RA = true, AD = false, CD = false, Seq(question), Seq(answer))
    }
}
