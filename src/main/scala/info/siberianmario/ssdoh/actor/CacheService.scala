package info.siberianmario.ssdoh.actor

import com.redis.RedisClient
import com.redis.api.StringApi.NX
import info.siberianmario.ssdoh.model.{Record, RecordPersistence}

import java.util.logging.Logger
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

class CacheService(redis: RedisClient, registryService: RegistryService, dnsService: DnsService)(implicit
  ec: ExecutionContext
) extends RecordPersistence {

  private val log = Logger.getLogger(getClass.getName)

  private def queryRecord(name: String): Future[Record] =
    registryService.getRecord(name).flatMap {
      case Some(record) => Future.successful(record)
      case None => dnsService.getDnsRecord(name, "A")
    }

  def getCachedRecord(name: String): Future[Record] =
    redis.get(name).map(value => toRecord(name -> value)) match {
      case Some(record) =>
        redis.ttl(name) match {
          case Some(ttl) =>
            val updatedRec = record.copy(ttl = ttl.toInt)
            log.info(s"Returning cached record: $updatedRec")
            Future.successful(updatedRec)
          case None =>
            log.warning(s"Failed to get TTL for $name")
            Future.failed(new IllegalStateException(s"Failed to get TTL for $name"))
        }
      case None =>
        queryRecord(name).map { record =>
          log.info(s"Inserting record into cache: $record")
          redis.set(name, fromRecord(record), NX, record.ttl.seconds)
          record
        }
    }
}
