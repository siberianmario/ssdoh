package info.siberianmario.ssdoh.actor

import com.redis.RedisClient
import info.siberianmario.ssdoh.model.{Record, RecordPersistence}

import java.util.logging.Logger
import scala.concurrent.{ExecutionContext, Future}

class RegistryService(redis: RedisClient)(implicit ec: ExecutionContext) extends RecordPersistence {

  private val log = Logger.getLogger(getClass.getName)

  def getAllRecords(): Future[Seq[Record]] = Future {
    redis.hgetall1(recordsTag) match {
      case Some(records) => records.map(toRecord).toSeq
      case None => Seq()
    }
  }

  def getRecord(name: String): Future[Option[Record]] = Future {
    redis.hget(recordsTag, name).map(value => toRecord(name -> value))
  }

  def createRecord(record: Record): Future[Record] = Future {
    redis.hset(recordsTag, record.name, fromRecord(record))
    log.info(s"Created record [${record.name}]")
    record
  }

  def updateRecord(record: Record): Future[Option[Record]] = Future {
    if (redis.hexists(recordsTag, record.name)) {
      redis.hset(recordsTag, record.name, fromRecord(record))
      log.info(s"Updated record [${record.name}]")
      Some(record)
    } else {
      log.info(s"Failed to find record to update [${record.name}]")
      None
    }
  }

  def deleteRecord(name: String): Future[Unit] = Future {
    redis.hdel(recordsTag, name)
    log.info(s"Deleted record [$name]")
  }
}
