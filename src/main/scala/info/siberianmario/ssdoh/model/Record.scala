package info.siberianmario.ssdoh.model

case class Record(name: String, data: String, ttl: Int)

case class UpdateRecord(data: String, ttl: Int)
