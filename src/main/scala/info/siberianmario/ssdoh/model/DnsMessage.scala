package info.siberianmario.ssdoh.model

import akka.util.ByteString

import scala.annotation.tailrec

final case class DnsMessage(header: DnsHeader, questions: Seq[DnsQuestion], answers: Seq[DnsAnswer])

final case class DnsHeader(
  id: Int = 0,
  qr: Boolean = false,
  opCode: Int = 0,
  aa: Boolean = false,
  tc: Boolean = false,
  rd: Boolean = true,
  ra: Boolean = false,
  ad: Boolean = false,
  cd: Boolean = false,
  rCode: Int = 0,
  qdCount: Int = 0,
  anCount: Int = 0,
  nsCount: Int = 0,
  arCount: Int = 0
)

final case class DnsQuestion(qName: String, qType: Int, qClass: Int)

final case class DnsAnswer(name: String, aType: Int, aClass: Int, ttl: Int, rdLength: Int, rData: Array[Byte])

object DnsMessage {

  implicit val byteOrder: java.nio.ByteOrder = java.nio.ByteOrder.BIG_ENDIAN

  def toBinary(message: DnsMessage): ByteString = {
    val builder = ByteString.newBuilder
    builder.append(buildHeader(message.header))
    message.questions.foreach { question =>
      builder.append(buildQuestion(question))
    }
    val responseStart = builder.result()
    message.answers.foreach { answer =>
      builder.append(buildAnswer(answer, responseStart))
    }

    builder.result()
  }

  def buildHeader(header: DnsHeader): ByteString = {
    val builder = ByteString.newBuilder
    builder.putShort(header.id)

    var flags = setFlag(0, 0, header.qr)
    flags = setFlag(flags, 5, header.aa)
    flags = setFlag(flags, 6, header.tc)
    flags = setFlag(flags, 7, header.rd)
    flags = setFlag(flags, 8, header.ra)
    flags = setFlag(flags, 10, header.ad)
    flags = setFlag(flags, 11, header.cd)
    flags = flags | ((header.opCode & 0x0f) << 11)
    flags = flags | (header.rCode & 0x0f)
    builder.putShort(flags)

    builder.putShort(header.qdCount)
    builder.putShort(header.anCount)
    builder.putShort(header.nsCount)
    builder.putShort(header.arCount)

    builder.result()
  }

  def setFlag(flags: Int, i: Int, value: Boolean): Int =
    if (value)
      flags | (1 << (15 - i))
    else
      flags & ~(1 << (15 - i))

  def buildQuestion(question: DnsQuestion): ByteString = {
    val builder = ByteString.newBuilder
    builder.append(buildLabels(question.qName))
    builder.putByte(0x00)
    builder.putShort(question.qType)
    builder.putShort(question.qClass)
    builder.result()
  }

  private def buildLabels(name: String) = {
    val builder = ByteString.newBuilder
    val labels = name.split("\\.")
    labels.foreach { label =>
      builder.putByte(label.length.toByte)
      builder.putBytes(label.getBytes)
    }
    builder.result()
  }

  def buildAnswer(answer: DnsAnswer, bytes: ByteString): ByteString = {
    val builder = ByteString.newBuilder
    val labels = buildLabels(answer.name)
    val index = bytes.indexOfSlice(labels)
    builder.putShort(0xc000 | index)
    builder.putShort(answer.aType)
    builder.putShort(answer.aClass)
    builder.putInt(answer.ttl)
    builder.putShort(answer.rdLength)
    builder.putBytes(answer.rData)
    builder.result()
  }

  def fromBinary(bytes: ByteString): DnsMessage = {
    val (header, headerTail) = parseHeader(bytes)
    val (questions, questionsTail) = parseQuestions(headerTail, header.qdCount)
    DnsMessage(header, questions, Seq())
  }

  def getFlag(flags: Int, i: Int): Boolean =
    (flags & (1 << (15 - i))) != 0

  def parseHeader(bytes: ByteString): (DnsHeader, ByteString) = {
    val flags = ((bytes(2) & 0xff) << 8) + (bytes(3) & 0xff)
    val header = DnsHeader(
      id = ((bytes(0) & 0xff) << 8) + (bytes(1) & 0xff),
      qr = getFlag(flags, 0),
      opCode = (flags >> 11) & 0x0f,
      aa = getFlag(flags, 5),
      tc = getFlag(flags, 6),
      rd = getFlag(flags, 7),
      ra = getFlag(flags, 8),
      ad = getFlag(flags, 10),
      cd = getFlag(flags, 11),
      rCode = flags & 0x0f,
      qdCount = (bytes(4) << 8) + bytes(5),
      anCount = (bytes(6) << 8) + bytes(7),
      nsCount = (bytes(8) << 8) + bytes(9),
      arCount = (bytes(10) << 8) + bytes(11)
    )
    (header, bytes.drop(12))
  }

  @tailrec
  def parseQuestions(
    bytes: ByteString,
    qdCount: Int,
    questions: Seq[DnsQuestion] = Seq()
  ): (Seq[DnsQuestion], ByteString) = {

    @tailrec
    def parseName(byteString: ByteString, acc: ByteString = ByteString.empty): (String, ByteString) = {
      val len = byteString(0)
      if (len == 0)
        (acc.drop(1).utf8String, byteString.drop(1))
      else {
        val (label, tail) = byteString.drop(1).splitAt(len)
        parseName(tail, acc ++ ByteString(".") ++ label)
      }
    }

    if (qdCount < 1)
      (questions, bytes)
    else {
      val (qName, tail) = parseName(bytes)
      val qType = (tail(0) << 8) + tail(1)
      val qClass = (tail(2) << 8) + tail(3)
      val question = DnsQuestion(qName, qType, qClass)
      parseQuestions(tail.drop(4), qdCount - 1, questions :+ question)
    }
  }
}
