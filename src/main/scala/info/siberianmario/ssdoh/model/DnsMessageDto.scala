package info.siberianmario.ssdoh.model

final case class DnsResponseDto(
  Status: Int,
  TC: Boolean,
  RD: Boolean,
  RA: Boolean,
  AD: Boolean,
  CD: Boolean,
  Question: Seq[DnsQuestionDto],
  Answer: Seq[DnsAnswerDto]
)

final case class DnsQuestionDto(name: String, `type`: Int)

final case class DnsAnswerDto(name: String, `type`: Int, TTL: Int, data: String)
