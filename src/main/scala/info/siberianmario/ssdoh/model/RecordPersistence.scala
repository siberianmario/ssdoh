package info.siberianmario.ssdoh.model

trait RecordPersistence {

  final val recordsTag = "records"

  def toRecord(tuple: (String, String)): Record = {
    val arr = tuple._2.split(",")
    Record(tuple._1, arr(0), Integer.parseInt(arr(1)))
  }

  def fromRecord(record: Record): String =
    record.data + "," + record.ttl
}
