package info.siberianmario.ssdoh.model

case class RecordType(code: Int, desc: String) {

  def equal(str: String): Boolean =
    if (desc == str)
      true
    else
      try Integer.parseInt(str) == code
      catch {
        case _: NumberFormatException => false
      }
}

object RecordType {

  val A = RecordType(1, "A")

  val types = Seq(A)

  def getCode(str: String): Option[Int] =
    types.find(_.equal(str)).map(_.code)
}
