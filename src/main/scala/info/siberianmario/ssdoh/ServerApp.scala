package info.siberianmario.ssdoh

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.redis.RedisClient
import info.siberianmario.ssdoh.actor.{CacheService, DnsQueryService, DnsService, RegistryService}
import info.siberianmario.ssdoh.router.RootEndpoint

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

object ServerApp extends App {

  implicit val system: ActorSystem = ActorSystem("ssdoh-server")
  implicit val executionContext: ExecutionContext = system.dispatcher

  private val redis = new RedisClient("redis", 6379)

  private val registry = new RegistryService(redis)
  private val dnsService = new DnsService()
  private val cache = new CacheService(redis, registry, dnsService)
  private val queryService = new DnsQueryService(cache)

  private val rootEndpoint = RootEndpoint(queryService, registry)

  private val serverBinding = Http().newServerAt("0.0.0.0", 8080).bindFlow(rootEndpoint.routes)

  serverBinding.onComplete {
    case Success(bound) =>
      println(s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}")
    case Failure(e) =>
      Console.err.println(s"Server could not start!")
      e.printStackTrace()
      system.terminate()
  }

  Await.result(system.whenTerminated, Duration.Inf)
}
