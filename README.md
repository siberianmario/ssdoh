# SSDOH

Simple Scala DNS over HTTPS server

[![pipeline status](https://gitlab.com/siberianmario/ssdoh/badges/develop/pipeline.svg)](https://gitlab.com/siberianmario/ssdoh/commits/develop)

## Build and deploy locally
In order to run you should have Docker Engine already [installed](https://docs.docker.com/install/)

Run `sbt docker:publishLocal` to build an image and then `docker-compose up -d` to run the server.