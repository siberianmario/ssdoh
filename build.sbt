organization    := "info.siberianmario"
name := "ssdoh"

version := "0.1"

scalaVersion := "2.13.12"

lazy val akkaHttpVersion = "10.2.10"
lazy val akkaVersion    = "2.6.20"

lazy val root = (project in file(".")).
  settings(
    name := "ssdoh",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,
      "net.debasishg"     %% "redisclient"          % "3.42",

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.2.15"        % Test,
      "org.scalamock"     %% "scalamock"            % "5.2.0"         % Test
    ),
    scalacOptions ++= Seq("-deprecation", "-Xfatal-warnings")
  )

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
enablePlugins(AshScriptPlugin)

Compile / mainClass := Some("info.siberianmario.ssdoh.ServerApp")

dockerBaseImage := "openjdk:jre-alpine"
dockerRepository := Some("siberianmario")